export const Cart = ({ cart }) => (
  <div className="bg-gray-50 pr-4 sm:pr-6 lg:pr-8 lg:flex-shrink-0 lg:border-l lg:border-gray-200 xl:pr-0">
    <div className="pl-6 lg:w-80">
      <div className="pt-6 pb-2">
        <h2 className="text-sm font-semibold">Cart</h2>
      </div>
      <div>
        <ul className="divide-y divide-gray-200">
          {Object.values(cart).map((item) => (
            <li className="py-4" key={item.imdbID}>
              <div className="flex space-x-3">
                <img
                  className="h-6 w-6 rounded"
                  src={item.Poster}
                  alt={item.Title}
                />
                <div className="flex-1 space-y-1">
                  <div className="flex items-center justify-between">
                    <h3 className="text-sm font-medium">{item.Title}</h3>
                  </div>
                </div>
              </div>
            </li>
          ))}
        </ul>
        <div className="py-4 text-sm border-t border-gray-200">
          <a
            href="#"
            className="text-indigo-600 font-semibold hover:text-indigo-900"
          >
            Checkout <span aria-hidden="true">&rarr;</span>
          </a>
        </div>
      </div>
    </div>
  </div>
)
