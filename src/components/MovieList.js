import { MovieCard } from './MovieCard'

export const MovieList = ({ movies, updateCart, cart }) => {
  return (
    <div className="flex-1 min-w-0 bg-white xl:flex">
      <div className="xl:flex-shrink-0 xl:w-64 xl:border-r xl:border-gray-200 bg-white" />
      <div className="bg-white lg:min-w-0 lg:flex-1">
        <div className="pl-4 pr-6 pt-4 pb-4 border-b border-t border-gray-200 sm:pl-6 lg:pl-8 xl:pl-6 xl:pt-6 xl:border-t-0">
          <div className="flex items-center">
            <h1 className="flex-1 text-lg font-medium">Movies</h1>
          </div>
        </div>
        {movies.status === 'pending' && (
          <div className="rounded-full border-4 border-t-4 border-gray-200 h-32 w-32 my-32 mx-auto">
            <div className="animate-spin rounded-full border-t-4 border-blue-500 h-32 w-32 -mt-1 -ml-1" />
          </div>
        )}
        {movies.status === 'rejected' && (
          <div className="grid grid-cols-2 gap-4 sm:grid-cols-3 p-4">
            <h1>{movies.error.message}</h1>
          </div>
        )}
        {movies.status === 'fulfilled' && (
          <ul className="grid grid-cols-2 gap-4 sm:grid-cols-3 p-4">
            {movies.data?.map((movie) => (
              <MovieCard
                key={movie.imdbID}
                movie={movie}
                updateCart={updateCart}
                cart={cart}
              />
            ))}
          </ul>
        )}
      </div>
    </div>
  )
}
