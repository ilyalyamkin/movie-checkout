import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import App from './App'

const mockMovies = [
  {
    Title: 'Fast & Furious 6',
    Year: '2013',
    imdbID: 'tt1905041',
    Type: 'movie',
    Poster: 'poster link',
  },
]

beforeAll(() => jest.spyOn(window, 'fetch'))

test('renders movies list', async () => {
  window.fetch.mockResolvedValueOnce({
    ok: true,
    json: async () => ({ Search: mockMovies }),
  })

  render(<App />)

  const movieCard = await screen.findByText(/Fast & Furious 6/i)
  expect(movieCard).toBeInTheDocument()
})

test('renders error message', async () => {
  window.fetch.mockResolvedValueOnce({
    ok: false,
    status: 401,
    json: async () => {
      throw new Error('Not authorized')
    },
  })

  render(<App />)

  const movieCard = await screen.findByText(/Not authorized/i)
  expect(movieCard).toBeInTheDocument()
})

test('adds movie to the cart', async () => {
  window.fetch.mockResolvedValueOnce({
    ok: true,
    json: async () => ({ Search: mockMovies }),
  })

  render(<App />)

  const addToCartButton = await screen.findByText(/Add to cart/i)
  userEvent.click(addToCartButton)

  // Find 2 titles: in cart and in list
  const cartItems = await screen.findAllByText(/Fast & Furious 6/i)
  expect(cartItems.length).toBe(2)
})

test('removes movie from the cart', async () => {
  window.fetch.mockResolvedValueOnce({
    ok: true,
    json: async () => ({ Search: mockMovies, success: true }),
  })

  render(<App />)

  const addToCartButton = await screen.findByText(/Add to cart/i)
  userEvent.click(addToCartButton)

  // Find 2 titles: in cart and in list
  const cartItems = await screen.findAllByText(/Fast & Furious 6/i)
  expect(cartItems.length).toBe(2)

  userEvent.click(await screen.findByText(/remove from cart/i))

  // Only 1 title left
  const allItems = await screen.findAllByText(/Fast & Furious 6/i)
  expect(allItems.length).toBe(1)
})
