import { useEffect, useState } from 'react'
import { Header } from './components/Header'
import { MovieList } from './components/MovieList'
import { Cart } from './components/Cart'
import { fetchMovies } from './api'
import { useDebounce } from './hooks/useDebounce'

const DEBOUNCE_DELAY = 300

const App = () => {
  const [searchTerm, setSearchTerm] = useState('fast')
  const [movies, setMovies] = useState({
    status: 'idle',
    error: null,
    data: null,
  })
  const [cart, setCart] = useState({})

  const debouncedSearchTerm = useDebounce(searchTerm, DEBOUNCE_DELAY)

  useEffect(() => {
    const fetchData = async () => {
      setMovies({ status: 'pending', error: null, data: null })
      try {
        const data = await fetchMovies(debouncedSearchTerm)
        setMovies({ status: 'fulfilled', error: null, data })
      } catch (error) {
        setMovies({ status: 'rejected', error, data: null })
      }
    }

    fetchData()
  }, [debouncedSearchTerm])

  return (
    <>
      <div
        className="fixed top-0 left-0 w-1/2 h-full bg-white"
        aria-hidden="true"
      ></div>
      <div
        className="fixed top-0 right-0 w-1/2 h-full bg-gray-50"
        aria-hidden="true"
      ></div>
      <div className="relative min-h-screen flex flex-col">
        <Header search={searchTerm} onSearch={setSearchTerm} />

        <div className="flex-grow w-full max-w-7xl mx-auto xl:px-8 md:flex">
          <MovieList movies={movies} cart={cart} updateCart={setCart} />
          <Cart cart={cart} />
        </div>
      </div>
    </>
  )
}

export default App
