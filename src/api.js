const API_KEY = 'a69aa69b'

export const fetchMovies = async (searchTerm) => {
  const endpoint = new URL(`http://www.omdbapi.com?apikey=${API_KEY}`)

  // Encode search params to avoid injection (s=fast&page=2)
  endpoint.searchParams.append('s', searchTerm)

  const response = await fetch(endpoint)
  const data = await response.json()
  return data.Search
}
