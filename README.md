# Movie Checkout

## Getting started

1. Install dependencies: `npm i`.
2. Run the app in the development mode: `npm run start`.
3. Run the tests: `npm run test`.
4. Build production app: `npm run build`.

## Features

- Mobile-first responsive design
- Integration tests with coverage over 80%
- Debouncing search input to avoid unnecessary requests
- Proper handling error/loading state

## What were the most difficult tasks?

Figuring out design and its implementation

## Did you learn anything new while completing this assignment?

Tailwind API is not great 🤷

## What did you not have time to add? What work took the up majority of your time?
## How could the application be improved?

To add:

- Search pagination
- The actual action on checkout
- Empty states implementation

To improve:

- Extract fetching logic into reducer + hook if used in more places
- Extract `cart` to `context`/`Provider` if used in different places/pages
- Typescript
